class Post < ActiveRecord::Base
	belongs_to :user
  has_many :comments, dependent: :destroy
  belongs_to :user
  belongs_to :keyword
end
